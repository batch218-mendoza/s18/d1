// console.log("Hello world");

/*
function printInfo(){
	let nickname = prompt("enter your nickname:");
console.log("Hi," + nickname);
}

printInfo(); // invoke*/

				//parameter
function printName(name){
	console.log("My name is " + name);
}
	    // argument
printName("juana");
printName("John");
printName("Cena");

//now we have a reusable function/ reusable task but could have diffrent output based on what value to process

//[SECTION] Parameters and Arguments

// Parameter
	//"name" is called a parameter
	// A "parameter" acts as name variable/container that exist only inside a function
	// it is used to store information that is is provided to a function when it is called/invoke


// Argument
	// "Juana", "John", and "Cena" the information/data provided directly into the function is called "argument."
	// Values passed when invoking a function are called arguments
	// these arguments are then stored as the parameter within the function


let sampleVariable = "Yui";

console.log(sampleVariable);

// Variable can also be passwed as an argument

// ----------------------------

function checkDivisibilityBy8(num){
	let remainder = num % 8;	
	console.log("The remainder of "+num+" divided by 8 is: " + remainder);
	let isDivisibilityBy8 = remainder === 0;
	console.log("is "+num+" divisible by 8?");
	console.log(isDivisibilityBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// function argument

function argumentFunction(){
	console.log("this function was passed as an argument before the message was printed.");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);
console.log(argumentFunction);

// ---------------------------------

//[SECTION] Using multiple parameters

	function createFullName(firstName, middleName, lastName){
		console.log("My Full Name is "+ firstName +" " +middleName +" " + lastName);
	}

	createFullName("Jan Jonell", "Yao", "Mendoza");

// using variable as an argument
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	function getDifferenceOf8Minus4(numA, numB){
		console.log("Difference: "+ (numA - numB));
	}

	getDifferenceOf8Minus4(8,4);
	//getDifferenceOf8Minus4(4,8); //this will resullt to logical error

// [SECTION] Return Statement

	// the return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called
function returnFullName(firstName, middleName, lastName){
	//codes after return does not execute
	return firstName + " " + middleName + " " + lastName;
}

let completeName = returnFullName("Jeffrey", "Smith", "Jordan");
console.log(completeName);

console.log("I am "+completeName);


function printPlayerInfo(userName, level, job){
	console.log("Username: " +userName);
	console.log("level: " +level);
	console.log("job: " +job);
}

let user1 = printPlayerInfo("boxsmapagmahal", "91", "swordsman");
console.log(user1); //returns undefined